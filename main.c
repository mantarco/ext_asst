/*
 * Copyright (C) 2015-2022 Ozan Egitmen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <windows.h>

#define HSIZE 32
#define BSIZE 8192
#define SSIZE 1024

#define CONT(var) (strstr(arg, var) != NULL)
#define NEWSTR(var) char* var = calloc(SSIZE, 1)
#define ADDSTR(var, var1, var2) NEWSTR(var); \
strcat(var, var1); \
strcat(var, var2)

#define CRYPT_END CryptReleaseContext(hProv, 0); \
CryptDestroyHash(hHash); \
CloseHandle(hFile)

void updateclient(const char cCurrentDirectory[]);
int extractfiles(const char arg[], const char cCurrentDirectory[]);
void defaultdirs();
void setversion(const char arg[]);
int hash(const char arg[]);

int main(int argc, char* argv[]) {
    char* arg = argv[argc - 1];

    int ret = 0;

    NEWSTR(cCurrentDirectory);
    int length = GetModuleFileNameA(NULL, cCurrentDirectory, SSIZE);
    cCurrentDirectory[length - 13] = 0;
    for (int i = 0; i < length - 13; i++) {
        if (cCurrentDirectory[i] == '\\') {
            cCurrentDirectory[i] = '/';
        }
        cCurrentDirectory[i] = tolower(cCurrentDirectory[i]);
    }

    if (CONT("updateclient")) {
        updateclient(cCurrentDirectory);
    } else if (CONT("extractfiles")) {
        ret = extractfiles(arg, cCurrentDirectory);
    } else if (CONT("defaultdirs")) {
        defaultdirs();
    } else if (CONT("setversion")) {
        setversion(arg);
    } else {
        ret = hash(arg);
    }

    return ret;
}

void updateclient(const char cCurrentDirectory[]) {
    Sleep(500);

    ADDSTR(cTemp, cCurrentDirectory, "/temp/MCo Tool.exe");
    ADDSTR(cTarget, cCurrentDirectory, "/MCo Tool.exe");

    MoveFileExA(cTemp, cTarget, MOVEFILE_REPLACE_EXISTING);

    NEWSTR(cStr);
    sprintf(cStr, "start \"MCo Tool\" \"%s/jre/bin/javaw.exe\" -jar \"%s\"", cCurrentDirectory, cTarget);
    system(cStr);
}

int extractfiles(const char arg[], const char cCurrentDirectory[]) {
    byte cSeperator = strrchr(arg, '/') - arg;

    NEWSTR(cSource);
    sprintf(cSource, "%s/temp%s.7z", cCurrentDirectory, &arg[cSeperator]);

    NEWSTR(cDest);
    memcpy(cDest, &arg[12], cSeperator - 12);

    NEWSTR(cSys);
    sprintf(cSys, "start \"\" /D \"%s\" /W /B 7za.exe x \"%s\" -o\"%s\" -pGk6pu2W4cPJ7V5CSYv4p -aoa", cCurrentDirectory, cSource, cDest);

    int exit = system(cSys);
    DeleteFileA(cSource);
    return exit;
}

void regkey(char* arg, const char cPath[], const char cKey[], const char cDef[]) {
    char cData[SSIZE];
    DWORD dwBufferSize = SSIZE;
    RegGetValueA(HKEY_LOCAL_MACHINE, cPath, cKey, RRF_RT_REG_SZ, NULL, (PVOID) &cData, &dwBufferSize);
    memcpy(arg, dwBufferSize == SSIZE ? cDef : cData, SSIZE);
}

void defaultdirs() {
    NEWSTR(cPath);
    NEWSTR(arg);
    regkey(cPath, "SOFTWARE\\WOW6432Node\\bohemia interactive\\arma 3", "main", "C:/Program Files (x86)/Steam/steamapps/common/Arma 3");
    regkey(arg, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\TeamSpeak 3 Client", "InstallLocation", "FAILED");
    if (CONT("FAILED")) {
        regkey(arg, "SOFTWARE\\TeamSpeak 3 Client", "", "C:/Program Files/TeamSpeak 3 Client");
    }

    printf("%s\n", cPath);
    printf("%s\n", arg);
}

void setversion(const char arg[]) {
    HKEY hKey;
    LSTATUS lStatus = RegOpenKeyExA(
            HKEY_LOCAL_MACHINE,
            "SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\567687fa-4342-43b8-ba9e-161808421667_is1",
            0,
            KEY_SET_VALUE,
            &hKey
    );

    if (lStatus == ERROR_SUCCESS) {
        RegSetValueExA(hKey, "DisplayVersion", 0, REG_SZ, &arg[10], strlen(&arg[9]));
    }
}

int hash(const char arg[]) {
    ADDSTR(cHashFile, arg, ".chash");

    HANDLE hHashFile;
    hHashFile = CreateFileA(cHashFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
    if (hHashFile != INVALID_HANDLE_VALUE) {
        char* cHash = calloc(HSIZE + 1, 1);
        ReadFile(hHashFile, cHash, HSIZE, NULL, NULL);
        CloseHandle(hHashFile);

        printf("%s\n", cHash);
        return 0;
    }

    HANDLE hFile = CreateFileA(arg, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);

    int status = 0;
    HCRYPTPROV hProv = 0;
    if (!CryptAcquireContextA(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
        status = GetLastError();
        printf("CryptAcquireContext failed: %d", status);
        CloseHandle(hFile);
        return status;
    }

    HCRYPTHASH hHash = 0;
    if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash)) {
        status = GetLastError();
        printf("CryptCreateHash failed: %d", status);
        CryptReleaseContext(hProv, 0);
        CloseHandle(hFile);
        return status;
    }

    BOOL bResult;
    BYTE bFile[BSIZE];
    DWORD dwRead = 0;
    while ((bResult = ReadFile(hFile, bFile, BSIZE, &dwRead, NULL))) {
        if (dwRead == 0) {
            break;
        }

        if (!CryptHashData(hHash, bFile, dwRead, 0)) {
            status = GetLastError();
            printf("CryptHashData failed: %d", status);
            CRYPT_END;
            return status;
        }
    }

    if (!bResult) {
        status = GetLastError();
        printf("ReadFile failed: %d", status);
        CRYPT_END;
        return status;
    }

    DWORD dwHash = HSIZE / 2;
    BYTE bHash[HSIZE / 2];
    char cDigits[] = "0123456789abcdef";
    char* cHash = calloc(HSIZE + 1, 1);
    if (CryptGetHashParam(hHash, HP_HASHVAL, bHash, &dwHash, 0)) {
        for (int i = 0, j = 0; i < HSIZE; i += 2, j++) {
            cHash[i] = cDigits[bHash[j] >> 4];
            cHash[i + 1] = cDigits[bHash[j] & 0xf];
        }
        printf("%s\n", cHash);
    } else {
        status = GetLastError();
        printf("CryptGetHashParam failed: %d", status);
    }

    CRYPT_END;

    if (CONT(".pbo") && !CONT(".bisign")) {
        hHashFile = CreateFileA(cHashFile, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        WriteFile(hHashFile, cHash, HSIZE, NULL, NULL);
        CloseHandle(hHashFile);
    }

    return status;
}
